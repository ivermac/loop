<?php

// ************************************* MAIN ROUTES ****************************************
Route::get('/', 'MainController@home');
Route::get('meta', function() { 

	/*$jskld= Schema::getColumnListing('users'); 
	return dd($jskld); */

});

Route::get('test/export', array('as' => 'test-export', 'uses' => 'MainController@_export'));
Route::get('test/import', array('as' => 'test-export', 'uses' => 'MainController@_importClients'));

//Unauthenticated group
Route::group(array('before' => 'guest'), function(){
	Route::get('/home', array('as' => 'home', 'uses' => 'MainController@home'));
	Route::post('/authenticate', array('as' => 'authenticate', 'uses' => 'MainController@authenticate') );
	Route::get('/user/activate/{code}', array('as' => 'user-activate', 'uses' => 'UserController@_activateaccount'));
	Route::controller('password', 'RemindersController');
});

//Authenticated Group
Route::group(array('before' => 'auth'), function(){
	// ************************************* SUPPORT ROUTES ****************************************
	Route::get('/support/delete/{id}', array('as' => 'support-delete', 'uses' => 'SupportController@delete'));
	Route::get('/support/edit/{id}', array('as' => 'support-edit', 'uses' => 'SupportController@edit'));
	Route::get('/support/new', array('as' => 'support-new', 'uses' => 'SupportController@_new'));
	Route::get('/support/dashboard', array('as' => 'support-dashboard', 'uses' => 'SupportController@_dashboard'));
	Route::post('/support/add-or-update', array('as' => 'support-add-or-update', 'uses' => 'SupportController@add_or_update'));
	Route::get('/support/list', array('as' => 'support-list', 'uses' => 'SupportController@_list'));
	Route::get('/support/view/{id}', array('as' => 'support-view', 'uses' => 'SupportController@view'));
	Route::get('/support/search', array('as' => 'support-search', 'uses' => 'SupportController@search'));
	Route::get('/support/report/daily', array('as' => 'support-daily-report', 'uses' => 'SupportController@daily_report'));
	Route::get('/support/report/weeky', array('as' => 'support-weekly-report', 'uses' => 'SupportController@weekly_report'));
	Route::get('/support/report/per-user-assigned', array('as' => 'support-per-user-assigned-report', 'uses' => 'SupportController@per_user_assigned'));
	Route::get('/support/report/per-ticket-assigned', array('as' => 'support-per-ticket-assigned-report', 'uses' => 'SupportController@per_ticket_assigned'));
	Route::get('/support/export', array('as' => 'support-export', 'uses' => 'SupportController@export'));

	// ************************************* CLIENT ROUTES ****************************************
	Route::get('/client/delete/{id}', array('as' => 'client-delete', 'uses' => 'ClientController@delete'));
	Route::get('/client/edit/{id}', array('as' => 'client-edit', 'uses' => 'ClientController@edit'));
	Route::get('/client/new', array('as' => 'client-new', 'uses' => 'ClientController@_new'));
	Route::post('/client/add-or-update', array('as' => 'client-add-or-update', 'uses' => 'ClientController@add_or_update'));
	Route::get('/client/list', array('as' => 'client-list', 'uses' => 'ClientController@_list'));
	Route::get('/client/view/{id}', array('as' => 'client-view', 'uses' => 'ClientController@view'));
	Route::get('/client/search', array('as' => 'client-search', 'uses' => 'ClientController@search'));

	// ************************************* COMPANY ROUTES ****************************************
	Route::get('/company/delete/{id}', array('as' => 'company-delete', 'uses' => 'CompanyController@delete'));
	Route::get('/company/edit/{id}', array('as' => 'company-edit', 'uses' => 'CompanyController@edit'));
	Route::get('/company/new', array('as' => 'company-new', 'uses' => 'CompanyController@_new'));
	Route::post('/company/add-or-update', array('as' => 'company-add-or-update', 'uses' => 'CompanyController@add_or_update'));
	Route::get('/company/list', array('as' => 'company-list', 'uses' => 'CompanyController@_list'));

	// ************************************* SERVICE ROUTES ****************************************
	Route::get('/service/delete/{id}', array('as' => 'service-delete', 'uses' => 'ServiceController@delete'));
	Route::get('/service/edit/{id}', array('as' => 'service-edit', 'uses' => 'ServiceController@edit'));
	Route::get('/service/new', array('as' => 'service-new', 'uses' => 'ServiceController@_new'));
	Route::post('/service/add-or-update', array('as' => 'service-add-or-update', 'uses' => 'ServiceController@add_or_update'));
	Route::get('/service/list', array('as' => 'service-list', 'uses' => 'ServiceController@_list'));

	// ************************************* SERVICE TYPE ROUTES ****************************************
	Route::get('/service-type/delete/{id}', array('as' => 'service-type-delete', 'uses' => 'ServiceTypeController@delete'));
	Route::get('/service-type/edit/{id}', array('as' => 'service-type-edit', 'uses' => 'ServiceTypeController@edit'));
	Route::get('/service-type/new', array('as' => 'service-type-new', 'uses' => 'ServiceTypeController@_new'));
	Route::post('/service-type/add-or-update', array('as' => 'service-type-add-or-update', 'uses' => 'ServiceTypeController@add_or_update'));
	Route::get('/service-type/list', array('as' => 'service-type-list', 'uses' => 'ServiceTypeController@_list'));

	// ************************************* USERS ROUTES ****************************************
	Route::get('/user/delete/{id}', array('as' => 'user-delete', 'uses' => 'UserController@delete'));
	Route::get('/user/edit/{id}', array('as' => 'user-edit', 'uses' => 'UserController@edit'));
	Route::get('/user/new', array('as' => 'user-new', 'uses' => 'UserController@_new'));
	Route::post('/user/add-or-update', array('as' => 'user-add-or-update', 'uses' => 'UserController@add_or_update'));
	Route::get('/user/list', array('as' => 'user-list', 'uses' => 'UserController@_list'));
	Route::get('/user/password', array('as' => 'user-change-password', 'uses' => 'UserController@_getnewpassword'));
	Route::post('/user/passwordchange', array('as' => 'user-password-change', 'uses' => 'UserController@_postnewpassword'));

	// ************************************* SITE CLASSIFICATION ROUTES  **************************************
	Route::get('/site/delete/{id}', array('as' => 'site-delete', 'uses' => 'SiteClassificationController@delete'));
	Route::get('/site/edit/{id}', array('as' => 'site-edit', 'uses' => 'SiteClassificationController@edit'));
	Route::get('/site/new', array('as' => 'site-new', 'uses' => 'SiteClassificationController@_new'));
	Route::post('/site/add-or-update', array('as' => 'site-add-or-update', 'uses' => 'SiteClassificationController@add_or_update'));
	Route::get('/site/list', array('as' => 'site-list', 'uses' => 'SiteClassificationController@_list'));

	//logout
	Route::get('/logout', array('as' => 'logout', 'uses' => 'MainController@logout') );
});