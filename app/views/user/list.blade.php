@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Support
        <small>Listing</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <table class="table">
        <thead>
            <tr>
                <th>User Name</th>
                <th>Mail</th>
                <th>Emp_no</th>
                <th>Company</th>
                <th>Description</th>
                <th>Role</th>
                <th>Active</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user as $row)
                <tr>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->emp_no }}</td>
                    <td>{{ $company[$row->company]  }}</td>
                    <td>{{ $row->description }}</td>
                    <td>{{ $role[$row->role]  }}</td>
                    <td>{{ (empty($row->is_active)) ? "No" : "Yes" }}</td>
                    <td>
                        @include('layouts.links', array('routes'=>['delete'=> 'user-delete',
                                                                   'edit' => 'user-edit',
                                                                   'view' => '#'], 'id' => $row->id))

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</section><!-- /.content -->
@stop
