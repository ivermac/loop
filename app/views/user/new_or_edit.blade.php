@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small>{{ (empty($user)) ? "New" : "Edit" }}</small>
        </h1>
    </section>

    <section class="content">
        {{ Form::open(array('route' => 'user-add-or-update', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            @if (isset($user))
            {{ Form::hidden('id', $user->id) }}
            @endif
          <div class="form-group">
              <label for="email" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-9">
                  {{ Form::text('email',
                                (empty($user->email)) ? '' : $user->email,
                                array('id'=>'email', 'placeholder'=>'jj@wekapesa.com', 'class'=>'form-control')) }}
                  {{ $errors->first('email') }}
              </div>
          </div>
          <div class="form-group">
              <label for="emp_no" class="col-sm-2 control-label">Emp No</label>
              <div class="col-sm-9">
                  {{ Form::text('emp_no',
                                (empty($user->emp_no)) ? '' : $user->emp_no,
                                array('id'=>'emp_no', 'placeholder'=>'EK0000', 'class'=>'form-control')) }}
                  {{ $errors->first('emp_no') }}
              </div>
          </div>
          <div class="form-group">
              <label for="password" class="col-sm-2 control-label">Password</label>
              <div class="col-sm-9">
                  {{ Form::password('password',
                                // (empty($user->password)) ? '' : $user->password,
                                '',
                                array('id'=>'password', 'class'=>'form-control')) }}
                    {{ $errors->first('password') }}
              </div>
          </div>
          <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-9">
                {{ Form::textarea('description',
                                (empty($user->description)) ? '' : $user->description,
                                array('id'=>'description', 'placeholder'=>'What you do?', 'class'=>'form-control', 'rows' => '3')) }}
                 {{ $errors->first('description') }}
            </div>
          </div>
          <div class="form-group">
            <label for="company" class="col-sm-2 control-label">Company</label>
            <div class="col-sm-9">
                {{ Form::select('company', 
                                $company,
                                (empty($user->company)) ? '' : $user->company,
                                array('id'=>'company', 'class'=>'form-control')) }}
               {{ $errors->first('company') }}
            </div>
          </div>
          <div class="form-group">
            <label for="role" class="col-sm-2 control-label">Role</label>
            <div class="col-sm-9">
                {{ Form::select('role', 
                                array('2' => 'Company Admin', '3' => 'Normal User'),
                                (empty($user->role)) ? '' : $user->role,
                                array('id'=>'role', 'class'=>'form-control')) }}
                 {{ $errors->first('Role') }}
            </div>
          </div>
          <div class="form-group">
              <label for="is_active" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-9">
              {{ Form::checkbox("is-active",
                                $value = 1,
                                $checked = (empty($user->is_active)) ? null : true,
                                array('id'=>'is-active')) }}
              </div>
          </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-flat">{{ (empty($service)) ? "Save" : "Update" }}</button>
                </div>
            </div>
        {{ Form::close() }}
    </section>

@stop

@section('custom-script')
    {{ HTML::script('js/icheck.min.js') }}
    <script type="text/javascript">
        $(function(){
            var checkbox_toggler = function() {
                    var is_active = "{{ (empty($user->is_active)) ? 0 : 1 }}";
                    if (is_active == 1) {
                        $('#is_active').iCheck('check');
                    } else {
                        $('#is_active').iCheck('uncheck');
                    }
                },
                init = function() {
                    checkbox_toggler();
                }();
        })
    </script>
@stop