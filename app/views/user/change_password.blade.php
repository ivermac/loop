@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small>Change Password</small>
        </h1>
    </section>
    <section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div><!-- /.box-header -->
          @if(isset($fail))
               <div class="alert alert-warning alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <strong>Alert!</strong> {{ $fail }}
               </div>
          @endif
          @if(isset($success))
               <div class="alert alert-success alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <strong>Alert!</strong> {{ $success }}
               </div>
          @endif

        {{ Form::open(array('route' => 'user-password-change', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
        {{ Form::token() }}
          <div class="form-group">
              <label for="password" class="col-sm-2 control-label">Current Password</label>
              <div class="col-sm-9">
                  {{ Form::password('password',
                                // (empty($user->password)) ? '' : $user->password,
                                '',
                                array('id'=>'password', 'class'=>'form-control')) }}
                    {{ $errors->first('password') }}
              </div>
          </div>
          <div class="form-group">
              <label for="password_new" class="col-sm-2 control-label">New Password</label>
              <div class="col-sm-9">
                  {{ Form::password('password_new',
                                '',
                                array('id'=>'password_new', 'class'=>'form-control')) }}
                    {{ $errors->first('password_new') }}
              </div>
          </div>
          <div class="form-group">
              <label for="password_new_confirm" class="col-sm-2 control-label">Confirm New Password</label>
              <div class="col-sm-9">
                  {{ Form::password('password_new_confirm',
                                '',
                                array('id'=>'password_new_confirm', 'class'=>'form-control')) }}
                    {{ $errors->first('password_new_confirm') }}
              </div>
          </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-flat">Submitt</button>
                </div>
            </div>
        {{ Form::close() }}
        </div>
    </section>
@stop