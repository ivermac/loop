@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Client
            <small>{{ (empty($client)) ? "New" : "Edit" }}</small>
        </h1>
    </section>
    <section class="content">
        {{ Form::open(array('route' => 'client-add-or-update', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            @if (isset($client))
            {{ Form::hidden('id', $client->id) }}
            @endif

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Support Details
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="circuit" class="col-sm-2 control-label">Circuit</label>
                                    <div class="col-sm-9">
                                        {{ Form::text('circuit',
                                                      (empty($client->circuit)) ? '' : $client->circuit,
                                                      array('id'=>'circuit', 'placeholder'=>'circuit', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="account" class="col-sm-2 control-label">Account</label>
                                    <div class="col-sm-9">
                                        {{ Form::text('account',
                                                      (empty($client->account)) ? '' : $client->account,
                                                      array('id'=>'account', 'placeholder'=>'account', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="client-name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-9">
                                        {{ Form::text('client-name',
                                                      (empty($client->account)) ? '' : $client->account,
                                                      array('id'=>'account', 'placeholder'=>'account', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-9">
                                        {{ Form::text('email',
                                                      (empty($client->name)) ? '' : $client->name,
                                                      array('id'=>'client-name', 'placeholder'=>'name', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-9">
                                    {{ Form::text('phone',
                                                  (empty($client->phone)) ? '' : $client->phone,
                                                  array('id'=>'phone', 'placeholder'=>'phone', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="business-contacts" class="col-sm-2 control-label">Business Contacts</label>
                                    <div class="col-sm-9">
                                    {{ Form::textarea('business-contacts',
                                                      (empty($client->business_contacts)) ? '' : $client->business_contacts,
                                                      array('placeholder'=>'business contacts', 'class'=>'form-control', 'id'=>'business-contacts', 'rows'=>'4')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="address" class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-9">
                                    {{ Form::text('address',
                                                  (empty($client->address)) ? '' : $client->address,
                                                  array('id'=>'address', 'placeholder'=>'address', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="zip" class="col-sm-2 control-label">Zip</label>
                                    <div class="col-sm-9">
                                        {{ Form::text('zip',
                                                      (empty($client->zip)) ? '' : $client->zip,
                                                      array('id'=>'zip', 'placeholder'=>'zip', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="company" class="col-sm-2 control-label">ISP</label>
                                    <div class="col-sm-9">
                                        {{ Form::select('company', $company_select,
                                                        (empty($client->company)) ? '' : $client->company,
                                                        array('id'=>'company', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="site" class="col-sm-2 control-label">Site</label>
                                    <div class="col-sm-9">
                                        {{ Form::select('site',
                                                        empty($site) ? "None" : $site,
                                                        (empty($client->site)) ? '' : $client->site,
                                                        array('id'=>'site', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="gps" class="col-sm-2 control-label">GPS</label>
                                    <div class="col-sm-9">
                                        {{ Form::text('gps',
                                                      (empty($client->gps)) ? '' : $client->gps,
                                                      array('id'=>'gps', 'placeholder'=>'gps', 'class'=>'form-control')) }}
                                    </div>
                                </div>
                                
                            </div>
                        
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Client Mapping Details
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="link" class="col-sm-2 control-label">Link</label>
                                    <div class="col-sm-9">
                                        {{ Form::text('link',
                                                      (empty($client->link)) ? '' : $client->link,
                                                      array('id'=>'link', 'placeholder'=>'link', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="backup-link" class="col-sm-2 control-label">Backup Link</label>
                                            <div class="col-sm-9">
                                                {{ Form::text('backup-link',
                                                              (empty($client->backup_link)) ? '' : $client->backup_link,
                                                              array('id'=>'backup-link', 'placeholder'=>'backup-link', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="router-model" class="col-sm-2 control-label">Router Model</label>
                                            <div class="col-sm-9">
                                                {{ Form::text('router-model',
                                                              (empty($client->router_model)) ? '' : $client->router_model,
                                                              array('id'=>'router-model', 'placeholder'=>'router model', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="service" class="col-sm-2 control-label">Service</label>
                                            <div class="col-sm-9">
                                                {{ Form::select('service',
                                                                empty($service) ? "None" : $service,
                                                                (empty($client->service)) ? '' : $client->service,
                                                                array('id'=>'service', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="switch-model" class="col-sm-2 control-label">Switch Model</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('switch-model',
                                                          (empty($client->switch_model)) ? '' : $client->switch_model,
                                                          array('id'=>'switch-model', 'placeholder'=>'switch model', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ip-address" class="col-sm-2 control-label">IP Address</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('ip-address',
                                                          (empty($client->ip_address)) ? '' : $client->ip_address,
                                                          array('id'=>'ip-address', 'placeholder'=>'172.2.0.3', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="subnet-mask" class="col-sm-2 control-label">IP Address</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('subnet-mask',
                                                          (empty($client->subnet_mask)) ? '' : $client->subnet_mask,
                                                          array('id'=>'subnet-mask', 'placeholder'=>'255.255.252', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="wifi" class="col-sm-2 control-label">WiFi</label>
                                            <div class="col-sm-9">
                                            {{ Form::checkbox("wifi",
                                                              $value = 1,
                                                              $checked = (empty($client->wifi)) ? null : true,
                                                              array('id'=>'wifi')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="wifi-device" class="col-sm-2 control-label">WiFi Device</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('wifi-device',
                                                          (empty($client->wifi_device)) ? '' : $client->wifi_device,
                                                          array('id'=>'wifi-device', 'placeholder'=>'WiFi device', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="building" class="col-sm-2 control-label">Building</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('building',
                                                          (empty($client->building)) ? '' : $client->building,
                                                          array('id'=>'building', 'placeholder'=>'Building', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="floor" class="col-sm-2 control-label">Floor</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('floor',
                                                          (empty($client->floor)) ? '' : $client->floor,
                                                          array('id'=>'floor', 'placeholder'=>'floor', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="wing" class="col-sm-2 control-label">Wing</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('wing',
                                                          (empty($client->wing)) ? '' : $client->wing,
                                                          array('id'=>'wing', 'placeholder'=>'wing', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="room" class="col-sm-2 control-label">Room</label>
                                            <div class="col-sm-9">
                                            {{ Form::text('room',
                                                          (empty($client->room)) ? '' : $client->room,
                                                          array('id'=>'room', 'placeholder'=>'room', 'class'=>'form-control')) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="is-active" class="col-sm-2 control-label">Active</label>
                                            <div class="col-sm-9">
                                            {{ Form::checkbox("is-active",
                                                              $value = 1,
                                                              $checked = (empty($client->is_active)) ? null : true,
                                                              array('id'=>'is-active')) }}
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Other Information
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                  <div class="form-group">
                                    <label for="other-info" class="col-sm-2 control-label">Other Info personal to the client</label>
                                    <div class="col-sm-9">
                                        {{ Form::textarea('other-info',
                                                          (empty($client->other_info)) ? '' : $client->other_info,
                                                          array('placeholder'=>'other Information Local to the Customer', 'class'=>'form-control', 'id'=>'other-info', 'rows'=>'7')) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                                </div>
                            </div>
                          </div> 
                      </div>
                    </div>

                  </div>
                </div>
    </section>
@stop

@section('custom-script')
    {{ HTML::script('js/icheck.min.js') }}
    <script type="text/javascript">
        $(function(){
            var checkbox_toggler = function() {
                    var wifi = "{{ (empty($client->wifi)) ? 0 : 1 }}",
                        is_active = "{{ (empty($client->is_active)) ? 0 : 1 }}";
                    if (wifi == 1) {
                        $('#wifi').iCheck('check');
                    } else {
                        $('#wifi').iCheck('uncheck');
                    }
                    if (is_active == 1) {
                        $('#is-active').iCheck('check');
                    } else {
                        $('#is-active').iCheck('uncheck');
                    }
                },
                init = function() {
                    checkbox_toggler();
                }();
        })
    </script>
@stop