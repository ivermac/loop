@extends('layouts.main')

@section('custom-style')
    {{ HTML::style('css/dataTables.bootstrap.css') }}
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Client
            <small>list</small>
        </h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Supportable Clients</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="clientTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Circuit</th>
                            <th>Account</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>Company</th>
                            <th>GPS</th>
                            <th>Has WiFi?</th>
                            <th>Is Active</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($client as $row)
                            <tr>
                                <td>{{ $row->circuit }}</td>
                                <td>{{ $row->account }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ str_limit($row->email, 10) }}</td>
                                <td>{{ $row->phone }}</td>
                                <td>{{ str_limit($row->address, 10) }}</td>
                                <td>{{  $companies[$row->company] }}</td>
                                {{-- Company::find($row->company)->name --}}
                                <td>{{ $row->gps }}</td>
                                <td>{{ (empty($row->wifi)) ? "No" : "Yes" }}</td>
                                <td>{{ (empty($row->is_active)) ? "No" : "Yes" }}</td>
                                <td>
                                    @include('layouts.links', array('routes'=>['delete'=> 'client-delete',
                                                                               'edit' => 'client-edit',
                                                                               'view' => 'client-view'], 'id' => $row->id))
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
                 {{ empty($client) ? "" : $client->links()}}
    </section>
@stop

@section('custom-script')
        
        <script type="text/javascript">
            $(function() {
                $("#clientTable").dataTable();
            });
        </script>
        {{ HTML::script('js/dataTables.bootstrap.js') }}
        <!-- Bootstrap -->
        {{ HTML::script('js/jquery.dataTables.js') }}

@stop