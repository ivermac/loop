@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Client View for <span class="badge"> {{ $client->name}} </span> 
           {{ HTML::link(
                URL::route('client-list', array('id'=>$client->id)),
                'Back',
                array(
                    'class' => 'glyphicon glyphicon-hand-left',
                )
            ) }}

            {{ HTML::link(
                URL::route('client-new', array('id'=>$client->id)),
                'New',
                array(
                    'class' => 'glyphicon glyphicon-pencil',
                )
            ) }}
        </h1>

    </section>

    <section class="content">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Personal Details
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <table class="table">  
                <thead>
                  <tr>
                      <th>Circuit</th>
                      <th>Account</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Company</th>
                      <th>Address</th>
                      <th>Is Active</th> 
                  </tr>
              </thead>
              <tbody>
                    <tr>
                        <td>{{ $client->circuit }}</td>
                        <td>{{ $client->account }}</td>
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ empty($client->company) ? "None" : Company::find($client->company)->name }}</td>
                        <td>{{ $client->address }}</td>
                        <td>{{ (empty($client->is_active)) ? "No" : "Yes" }}</td>
                    </tr>
              </tbody>
          </table>

              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Mapping Details
                </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                
                <table class="table">
                  <thead>
                      <tr>
                          <th>Link</th>
                          <th>Back Up Link</th>
                          <th>Router Model</th>
                          <th>Switch Model</th>
                          <th>IP Address</th>
                          <th>GPS</th>
                          <th>Has WiFi?</th>
                          <th>WiFI Device</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>{{ $client->link }}</td>
                          <td>{{ $client->backup_link }}</td>
                          <td>{{ $client->router_model }}</td>
                          <td>{{ $client->switch_model }}</td>
                          <td>{{ $client->ip_address. " / ".$client->subnet_mask }} </td>
                          <td>{{ $client->gps }}</td>
                          <td>{{ (empty($client->wifi)) ? "No" : "Yes" }}</td>
                          <td>{{ $client->wifi_device }}</td>
                      </tr>
                  </tbody>
              </table>

              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Other Information
                </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                <table class="table">
                  <thead>
                      <tr>
                          <th>Business Contacts</th>
                          <th>Building</th>
                          <th>Floor</th>
                          <th>Wing</th>
                          <th>Room</th>
                          <th>added By</th>
                          <th>Tools</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>{{ $client->business_contacts }}</td>
                          <td>{{ $client->building }}</td>
                          <td>{{ $client->floor }}</td>
                          <td>{{ $client->wing }}</td>
                          <td>{{ $client->room }} </td>
                          <td>{{ 'user' }}</td>
                          <td>
                            {{ HTML::link(
                                URL::route('client-delete', array('id'=>$client->id)),
                                'Delete',
                                array(
                                    'class' => 'client-delete',
                                )
                            ) }} |
                            {{ HTML::link(
                                URL::route('client-edit', array('id'=>$client->id)),
                                'Update',
                                array(
                                    'class' => 'client-edit',
                                )
                            ) }} |
                            {{ HTML::link(
                                URL::route('client-view', array('id'=>$client->id)),
                                'View',
                                array(
                                    'class' => 'client-view',
                                )
                            ) }}

                        </td>
                      </tr>
                  </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
        
    </section>

    <section class="content">
      <div class="col-sm-offset-2 col-sm-10">
          
            {{ HTML::link(
                URL::route('client-edit', array('id'=>$client->id)),
                'Edit',
                array(
                    'class' => 'btn btn-info btn-flat',
                )
            ) }} 
          
      </div>
    </section>
@stop