@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Company
        <small>Listing</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <table class="table">
        <thead>
            <tr>
                <th>Company Name</th>
                <th>Email</th>
                <th>Image</th>
                <th>Contact Details</th>
                <th>Description</th>
                <th>Active</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($company as $row)
                <tr>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->email }}</td>
                    <td>{{ $row->image }}</td>
                    <td>{{ $row->contact_details }}</td>
                    <td>{{ $row->description }}</td>
                    <td>{{ empty($row->is_active) ? "No" : "yes" }}</td>
                    <td>
                        @include('layouts.links', array('routes'=>['delete'=> 'company-delete',
                                                                   'edit' => 'company-edit',
                                                                   'view' => '#'], 'id' => $row->id))
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ empty($company) ? "" : $company->links()}}
</section><!-- /.content -->
@stop
