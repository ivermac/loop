@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company
            <small>{{ (empty($company)) ? "New" : "Edit" }}</small>
        </h1>
    </section>

    <section class="content">
        {{ Form::open(array('route' => 'company-add-or-update', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            @if (isset($company))
            {{ Form::hidden('id', $company->id) }}
            @endif

           <div class="form-group">
            <label for="name" class="col-sm-2 control-label">ISP *</label>
            <div class="col-sm-9">
                {{ Form::text('name',
                                  (empty($company->name)) ? Input::old('name') : $company->name,
                                  array('placeholder'=>'Name', 'class'=>'form-control', 'id'=>'name')) }}
            {{ $errors->first('name') }}
            </div>
          </div>

          <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email *</label>
            <div class="col-sm-9">
                {{ Form::text('email',
                                  (empty($company->email)) ? Input::old('email') : $company->email,
                                  array('placeholder'=>'Email', 'class'=>'form-control', 'id'=>'email')) }}
            {{ $errors->first('email') }}
            </div>
          </div>

          <div class="form-group">
            <label for="users_no" class="col-sm-2 control-label">Number of Users</label>
            <div class="col-sm-9">
                {{ Form::selectRange('users_no', 
                                1,200,
                                (empty($company->no_of_users)) ? Input::old('users_no') : $company->no_of_users,
                                array('id'=>'users_no', 'class'=>'form-control')) }}
            </div>
          </div>

          <div class="form-group">
            <label for="image" class="col-sm-2 control-label">Image</label>
            <div class="col-sm-9">
                {{ Form::text('image',
                                  (empty($company->image)) ? Input::old('image') : $company->image,
                                  array('placeholder'=>'Image', 'class'=>'form-control', 'id'=>'image')) }}
            </div>
          </div>


          <div class="form-group">
            <label for="contact_details" class="col-sm-2 control-label">Contact Details</label>
            <div class="col-sm-9">
                {{ Form::text('contact_details',
                                  (empty($company->contact_details)) ? Input::old('contact_details') : $company->contact_details,
                                  array('placeholder'=>'Contact Details', 'class'=>'form-control', 'id'=>'contact_details')) }}
            </div>
          </div>


          <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-9">
                {{ Form::text('description',
                                  (empty($company->description)) ? Input::old('description') : $company->description,
                                  array('placeholder'=>'Description', 'class'=>'form-control', 'id'=>'description')) }}
            </div>
          </div>

          <div class="form-group">
              <label for="is-active" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-9">
              {{ Form::checkbox('is-active',
                                $value = 1,
                                $checked = (empty($client->is_active)) ? Input::old('is-active') : true,
                                array('id'=>'is-active')) }}
              </div>
          </div>
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary btn-flat">{{ (empty($support)) ? "Save" : "Update" }}</button>
            </div>
          </div>
        {{ Form::close() }}
    </section>

@stop

@section('custom-script')
    <script type="text/javascript">
        $(function(){
            var checkbox_toggler = function() {
                    var is_active = "{{ (empty($company->is_active)) ? 0 : 1 }}";
                    if (is_active == 1) {
                        $('#is_active').iCheck('check');
                    } else {
                        $('#is_active').iCheck('uncheck');
                    }
                },
                init = function() {
                    checkbox_toggler();
                }();
        })
    </script>
@stop