@if (in_array(Auth::user()->role,[1,2]))
{{ HTML::link(
    ($routes['delete'] == "#") ? "#" : URL::route($routes['delete'], array('id'=>$id)),
    'Delete',
    array(
        'class' => $routes['delete'],
    )
) }} |
{{ HTML::link(
    ($routes['edit'] == "#") ? "#" : URL::route($routes['edit'], array('id'=>$id)),
    'Update',
    array(
        'class' => $routes['edit'],
    )
) }} |
@endif
{{ HTML::link(
    ($routes['view'] == "#") ? "#" : URL::route($routes['view'], array('id'=>$id)),
    'View',
    array(
        'class' => $routes['view'],
    )
) }}