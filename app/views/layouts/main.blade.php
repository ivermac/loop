
<!DOCTYPE html>
<html>
<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("50b80eca43480cc7d48cf698c8df6f82");</script><!-- end Mixpanel -->
    <head>
        <meta charset="UTF-8">
        <title>Loop| Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {{ HTML::style('css/bootstrap/css/bootstrap.min.css') }}
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        {{ HTML::style('css/AdminLTE.css') }}
        @yield('custom-style')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="{{ URL::route('support-list')}} " class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Loop
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Company Admin View<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    {{ HTML::image(URL::asset('img/empire.jpg'), '...', array('class'=>'img-circle')) }}
                                    <p>
                                        Admin 
                                        <small>Empire Micro Systems</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        <a href="{{ URL::route('user-change-password') }}" class="btn btn-default btn-flat">Change Password</a>
                                    </div>
                                    <div class="pull-right">
                                        {{ HTML::link('logout', 'Sign Out', array('class' => 'btn btn-default btn-flat')) }}
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            @section('left-side')
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                        {{ HTML::image(URL::asset('img/empire.jpg'), '...', array('class'=>'img-circle')) }}
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Empire Admin</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    @include('layouts.search')
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Support</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::route('support-list') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> List') }}</a></li>
                                <li><a href="{{ URL::route('support-new') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> New') }}</a></li>
                                <li><a href="{{ URL::route('support-dashboard') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> Dashboard') }}</a></li>
                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-user"></i>
                                        <span>Reports</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li><a href="{{ URL::route('support-daily-report') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> Daily') }}</a></li>
                                        <li><a href="{{ URL::route('support-weekly-report') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> Weekly') }}</a></li>
                                        <li><a href="{{ URL::route('support-per-user-assigned-report') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> Tickets assiged per user') }}</a></li>
                                        <li><a href="{{ URL::route('support-per-ticket-assigned-report') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> User assigned per ticket') }}</a></li>
                                    </ul>    
                                </li>
                            </ul>

                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>Clients</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::route('client-list') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> List') }}</a></li>
                                <li><a href="{{ URL::route('client-new') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> New') }}</a></li>
                            </ul>    
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-group"></i>
                                <span>Company</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::route('company-list') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> List') }}</a></li>
                                <li><a href="{{ URL::route('company-new') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> New') }}</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-credit-card"></i>
                                <span>Services</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::route('service-list') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> List') }}</a></li>
                                <li><a href="{{ URL::route('service-new') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> New') }}</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-briefcase"></i>
                                <span>Service Type</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::route('service-type-list') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> List') }}</a></li>
                                <li><a href="{{ URL::route('service-type-new') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> New') }}</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a>
                                <i class="fa fa-bullseye"></i>
                                <span>Site Classification</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::route('site-list') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> List') }}</a></li>
                                <li><a href="{{ URL::route('site-new') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> New') }}</a></li>
                            </ul>
                        </li>
                        @if (in_array(Auth::user()->role,[1]))
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>User</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::route('user-list') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> List') }}</a></li>
                                <li><a href="{{ URL::route('user-new') }}">{{ HTML::decode('<i class="fa fa-angle-double-right"></i> New') }}</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            @show


            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                @yield('content')                
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 1.9.1 -->
        {{ HTML::script('js/jquery-1.9.1.min.js') }}
        <!-- Bootstrap -->
        {{ HTML::script('js/bootstrap.min.js') }}
        <!-- AdminLTE App -->
        {{ HTML::script('js/AdminLTE/app.js') }}

        @yield('custom-script')
        <script type="text/javascript">
            $(function() {
                mixpanel.track("Video play");
            });

        </script>

    </body>
</html>
