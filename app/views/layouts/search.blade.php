{{ Form::open(array('route' => 'client-search' ? 'client-search' : $search, 'method' => 'get', 'role' => 'form', 'class' => 'sidebar-form')) }}
	<div class="input-group">
        {{ Form::input('search', 'q', null, ['placeholder' => 'Client CI....', 'class' => 'form-control']) }}
        <span class="input-group-btn">
            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
    </div>
{{ Form::close() }}