@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Service Type
        <small>Listing</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Service Speed</th>
                <th>Active</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($service_type as $row)
                <tr>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->service_speed }}</td>
                    <td>{{ empty($row->is_active) ? "No" : "yes" }}</td>
                    <td>
                        @include('layouts.links', array('routes'=>['delete'=> 'service-type-list',
                                                                   'edit' => 'service-type-edit',
                                                                   'view' => 'service-type-list'], 'id' => $row->id))
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ empty($service_type) ? "" : $service_type->links()}}
</section><!-- /.content -->
@stop
