@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Service Type
            <small>{{ (empty($service_type)) ? "New" : "Edit" }}</small>
        </h1>
    </section>

    <section class="content">
        {{ Form::open(array('route' => 'service-type-add-or-update', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            @if (isset($service_type))
            {{ Form::hidden('id', $service_type->id) }}
            @endif
          
          <div class="form-group">
              <label for="name" class="col-sm-2 control-label">Name *</label>
              <div class="col-sm-9">
                  {{ Form::text('name',
                                (empty($service_type->name)) ? Input::old('name') : $service_type->name,
                                array('id'=>'name', 'placeholder'=>'MPLS', 'class'=>'form-control')) }}
               {{ $errors->first('name')}}
              </div>
          </div>
          <div class="form-group">
            <label for="service-speed" class="col-sm-2 control-label">Service Speed Kbps *</label>
            <div class="col-sm-9">
                {{ Form::selectRange('service-speed', 
                                256,100000, 
                                (empty($service_type->service_speed)) ? Input::old('service-speed') : $service_type->service_speed,
                                array('id'=>'service-speed', 'class'=>'form-control')) }}
            </div>
          </div>
          <div class="form-group">
              <label for="is-active" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-9">
              {{ Form::checkbox("is-active",
                                $value = 1,
                                $checked = (empty($service_type->is_active)) ? null : true,
                                array('id'=>'is-active')) }}
              </div>
          </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-flat">{{ (empty($service_type)) ? "Save" : "Update" }}</button>
                </div>
            </div>
        {{ Form::close() }}
    </section>

@stop

@section('custom-script')
    {{ HTML::script('js/icheck.min.js') }}
    <script type="text/javascript">
        $(function(){
            var checkbox_toggler = function() {
                    var is_active = "{{ (empty($service_type->is_active)) ? 0 : 1 }}";
                    if (is_active == 1) {
                        $('#is_active').iCheck('check');
                    } else {
                        $('#is_active').iCheck('uncheck');
                    }
                },
                init = function() {
                    checkbox_toggler();
                }();
        })
    </script>
@stop