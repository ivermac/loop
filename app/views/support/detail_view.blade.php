@extends('layouts.main')

@section('content')
    <section class="content-header">
        
        <h1>
           Support Detail View for  <span class="badge"> {{ 'Ticket '. $support->ticket}} </span>
           Status 
          @if($support->status ==1)
            <button class="btn btn-warning">
                {{ 'Pending' }}
            </button>
          @else
            <button class="btn btn-primary">
                {{ 'Resolved' }}
            </button>
          @endif
        </h1>
    </section>
    
    <section class="content">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Support Details
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <table class="table">  
                <thead>
                  <tr>
                      <th>Customer</th>
                      <th>Escalated At</th>
                      <th>Resolved At</th>
                      <th>Incident</th>
                      <th>Resolution</th> 
                  </tr>
              </thead>
              <tbody>
                    <tr>
                        <td>{{ $support->customer }}</td>
                        <td>{{ $support->created_at }}</td>
                        <td>{{ $support->updated_at }}</td>
                        <td>{{ $support->incident }}</td>
                        <td>{{ $support->resolution }}</td>
                    </tr>
              </tbody>
          </table>

              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Site Details
                </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <table class="table">
                  <thead>
                      <tr>
                          <th>Site</th>
                          <th>Amount Payable</th>
                          <th>Client Phone</th>
                          <th>Client Address</th>
                          <th>Company From</th>
                          <th>GPS</th>
                          <th>Building</th>
                          <th>Floor</th>
                          <th>Wing</th>
                          <th>Room</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>{{ $client_site_details->region }}</td>
                          <td>{{ $client_site_details->price }}</td>
                          <td>{{ $client_details->phone }}</td>
                          <td>{{ $client_details->address }}</td>
                          <td>{{ $company[$client_details->company] }}</td>
                          <td>{{ $client_details->gps }}</td>
                          <td>{{ $client_details->building }}</td>
                          <td>{{ $client_details->floor}}</td>
                          <td>{{ $client_details->wing }}</td>
                          <td>{{ $client_details->room}}</td>
                      </tr>
                  </tbody>
              </table>
                

              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Client Mapping Details
                </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                  <table class="table">
                    <thead>
                        <tr>
                            <th>Link</th>
                            <th>Back Up Link</th>
                            <th>Router Model</th>
                            <th>Switch Model</th>
                            <td>Service</td>
                            <td>Access Technology</td>
                            <td>Speeds</td>
                            <th>IP Address</th>
                            <th>GPS</th>
                            <th>Has WiFi?</th>
                            <th>WiFI Device</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ empty($client_details->link) ? "None" : $client_details->link }}</td>
                            <td>{{ empty($client_details->backup_link) ? "None" : $client_details->backup_link }}</td>
                            <td>{{ empty($client_details->router_model) ? "None" : $client_details->router_model }}</td>
                            <td>{{ empty($client_details->switch_model) ? "None" : $client_details->switch_model }}</td>
                            <td>{{ empty($client_service->name) ? "None" : $client_service->name }}</td>
                            <td>{{ empty($client_details) ? "None" : $access_types[$client_details->service] }}</td>
                            <td>{{ empty($client_service->service_speed) ? "None" : $client_service->service_speed }}</td>
                            <td>{{ empty($client_details->ip_address) ? "None" : $client_details->ip_address." / ".$client_details->subnet_mask }} </td>
                            <td>{{ empty($client_details->gps) ? "None" : $client_details->gps  }}</td>
                            <td>{{ (empty($client_details->wifi)) ? "No" : "Yes" }}</td>
                            <td>{{ empty($client_details->wifi_device) ? "None" : $client_details->wifi_device }}</td>
                        </tr>
                    </tbody>
                </table> 
              </div>
            </div>
          </div>
        </div>
        
    </section>

    <section class="content">
      <div class="col-sm-offset-2 col-sm-10">
          
            {{ HTML::link(
                URL::route('support-edit', array('id'=>$support->id)),
                'Edit',
                array(
                    'class' => 'btn btn-info btn-flat',
                )
            ) }} 
          
      </div>
    </section>
@stop