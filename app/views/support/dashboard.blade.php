@extends('layouts.main')

@section('custom-style')
{{ HTML::style('css/morris.css') }}
@stop

@section('content')

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Number of Support Tickets Per Company</h3>
    </div>
    <div class="box-body chart-responsive">
        <div id="clients-per-company-support" style="height: 300px;"></div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
@stop


@section('custom-script')
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
    {{ HTML::script('js/raphael-min.js') }}
    {{ HTML::script('js/morris.min.js') }}
    <script type="text/javascript">
        $(function(){
            var lineGraph = function(data) {
                    new Morris.Bar({
                        element: 'clients-per-company-support',
                        data: data,
                         barColors: ['#00a65a'],
                        xkey: 'name',
                        ykeys: ['clients'],
                        labels: ['Total']
                    });
                },
                init = function() {
                    var data_list = []
                    $.each({{ $supported_clients }}, function(a, b) {
                        data_list.push({ clients: parseInt(b.client_count), name: b.name});
                    })
                    lineGraph(data_list);
                }();
        })
    </script>
@stop