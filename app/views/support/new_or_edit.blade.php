@extends('layouts.main')

@section('custom-style')
{{-- HTML::style('css/datepicker3.css') --}}
{{ HTML::style('css/chosen.min.css') }}
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Support
            <small>{{ (empty($support)) ? "New" : "Edit" }}</small>
        </h1>
    </section>

    <section class="content">
        {{ Form::open(array('route' => 'support-add-or-update', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            @if (isset($support))
            {{ Form::hidden('id', $support->id) }}
            @endif
          <div class="form-group">
            <label for="customer" class="col-sm-2 control-label">Customer</label>
            <div class="col-sm-9">
                {{ Form::select('customer', 
                                empty($customer) ? 'None: Make new' : $customer,
                                (empty($support->customer)) ? '' : $support->customer,
                                array('id'=>'customer', 'class'=>'form-control')) }}
                {{ $errors->first('customer')}}
            </div>
          </div>

          <div class="form-group">
            <label for="user" class="col-sm-2 control-label">Escalated to</label>
            <div class="col-sm-9">
                {{ Form::select('user', 
                                empty($users) ? 'None: Make new' : $users,
                                (empty($support->user)) ? '' : $support->user,
                                array('id'=>'user', 'class'=>'form-control')) }}

                {{ $errors->first('user')}}
            </div>
          </div>
          
          <div class="form-group">
            <label for="service" class="col-sm-2 control-label">Service</label>
            <div class="col-sm-9">
               {{ Form::select('service', 
                            empty($service) ? 'None: Make new' : $service,
                            (empty($support->service)) ? '' : $support->service,
                            array('id'=>'service', 'class'=>'form-control')) }}

                {{ $errors->first('service')}}
            </div>
          </div>
           <div class="form-group">
            <label for="incident" class="col-sm-2 control-label">Incident *</label>
            <div class="col-sm-9">
                {{ Form::textarea('incident',
                                  (empty($support->incident)) ? '' : $support->incident,
                                  array('placeholder'=>'Incident', 'class'=>'form-control', 'id'=>'incident', 'rows'=>'6')) }}
                    {{ $errors->first('incident') }}
            </div>
          </div>
          <div class="form-group">
              <label for="incident_tag" class="col-sm-2 control-label">incident_tag</label>
              <div class="col-sm-9">
                    {{ Form::select('incident_tag', 
                                array('Power' => 'Power', 'MisConfiguration' => 'MisConfiguration','Equipment' => 'Equipment', 'Transmission' => 'Transmission','Cable' => 'Cable','Problem X' => 'Problem X'),
                                (empty($support->incident_tag)) ? Input::old('incident_tag') : $support->incident_tag,
                                array('id'=>'incident_tag', 'class'=>'chosen-select form-control','tabindex' => 8, 'multiple' => true, 'data-placeholder'=> ' Search Tag')) }}
                    {{ $errors->first('incident_tag') }}
              </div>
          </div>
          @if(!empty($support))
          <div class="form-group">
            <label for="resolution" class="col-sm-2 control-label">Resolution</label>
            <div class="col-sm-9">
                {{ Form::textarea('resolution',
                                  (empty($support->resolution)) ? '' : $support->resolution,
                                  array('placeholder'=>'Resolution', 'class'=>'form-control', 'id'=>'resolution', 'rows'=>'6')) }}

                {{ $errors->first('resolution')}}
            </div>
          </div>
          <div class="form-group">
              <label for="status" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-9">
                  {{ Form::select('status', 
                                array('1' => 'Pending', '2' => 'Resolved'),
                                (empty($service->status)) ? Input::old('status') : $service->status,
                                array('id'=>'status', 'class'=>'form-control')) }}
              {{ $errors->first('status') }}
              </div>
          </div>
          @endif
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary btn-flat">Save</button>
            </div>
          </div>
        {{ Form::close() }}
    </section>

@stop

@section('custom-script')
    {{-- HTML::script('js/bootstrap-datepicker.js') --}}
    {{ HTML::script('js/chosen.jquery.min.js') }}
    {{-- HTML::script('js/chosen.proto.min.js') --}}
    <script type="text/javascript">
        $(function(){
            var setDatePicker = function() {
                    $('.input-group.date').datepicker({
                        format: 'yyyy-mm-dd',
                    });
                },
                setChosenConfig = function() {
                  var
                     config = {
                         '.chosen-select'           : {},
                         '.chosen-select-deselect'  : { allow_single_deselect: true },
                         '.chosen-select-no-single' : { disable_search_threshold: 10 },
                         '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                         '.chosen-select-width'     : { width: "95%" }
                     }, selector;
                 for (selector in config) {
                     if (config.hasOwnProperty(selector)) {
                         $(selector).chosen(config[selector]);
                     }
                 }
                },
                init = function() {
                   //setDatePicker();
                   
                    setChosenConfig();
                }();
        })
    </script>
@stop
