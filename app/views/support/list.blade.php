@extends('layouts.main')

@section('custom-style')
    {{ HTML::style('css/dataTables.bootstrap.css') }}
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Support
        <small>Listing</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="box">
            <div class="box-header">
                <h3 class="box-title">Supportable Clients</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="supportTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Ticket No</th>
                            <th>Customer</th>
                            <th>Assigned To</th>
                            <th>Escalation Date</th>
                            <th>Resolution Date</th>
                            <th>Status</th>
                            <th>Tags</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($support->count())
                        @foreach ($support as $row)
                            <tr>
                                <td>{{ $row->ticket }}</td>
                                <td>{{ str_limit($customers[$row->customer], 20) }}</td>
                                <td>{{ str_limit($users[$row->user], 20) }}</td>
                                <td>{{ $row->created_at }}</td>
                                <td>{{ $row->updated_at }}</td>
                                <td>
                                    @if($row->status ==1)
                                        {{ 'Pending'}}

                                    @else
                                        {{ 'Resolved' }}
                                    @endif
                                </td>
                                <td> {{ $row->incident_tag }} </td>
                                <td>
                                    @include('layouts.links', array('routes'=>['delete'=> 'support-list',
                                                                               'edit' => 'support-edit',
                                                                               'view' => 'support-view'], 'id' => $row->id))
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <p>No records Found</p>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    {{ empty($support) ? "" : $support->links()}}
</section><!-- /.content -->
@stop

@section('custom-script')
        
        <script type="text/javascript">
            $(function() {
                $("#supportTable").dataTable();
            });
        </script>
        {{ HTML::script('js/dataTables.bootstrap.js') }}
        <!-- Bootstrap -->
        {{ HTML::script('js/jquery.dataTables.js') }}

@stop