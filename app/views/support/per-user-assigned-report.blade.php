@extends('layouts.main')

@section('custom-style')
    {{ HTML::style('css/daterangepicker-bs3.css') }}
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Support
        <small>Per user assigned report</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
<a href=" {{ URL::route('support-export') }}" class="btn btn-primary pull-right" style="margin-right: 5px;"> <i class="fa fa-download"></i> Export Report</a>
<!-- Date and time range -->
<div class="form-group">
    <label>Date and time range:</label>
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-clock-o"></i>
        </div>
        <input type="text" class="form-control pull-right" id="reservationtime"/>
    </div><!-- /.input group -->
</div><!-- /.form group -->
<table class="table">
    <thead>
        <tr>
            <th>Email of Assignee</th>
            <th>Total Assigned Issues </th>
        </tr>
    </thead>
    <tbody>
        @foreach($results as $row)
        <tr>
            <td>{{ $row->email }}</td>
            <td>{{ $row->total_issues }}</td>
        </tr>
        @endforeach
        
    </tbody>
</table>
</section>
@stop

@section('custom-script')
        
        <script type="text/javascript">
            $(function() {
                $("#supportTable").dataTable();
                //Date range picker with time picker   
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            });
        </script>
        {{ HTML::script('js/dataTables.bootstrap.js') }}
        <!-- Bootstrap -->
        {{ HTML::script('js/jquery.dataTables.js') }}
        {{ HTML::script('js/daterangepicker.js') }}

@stop