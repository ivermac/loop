@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Service
            <small>{{ (empty($service)) ? "New" : "Edit" }}</small>
        </h1>
    </section>

    <section class="content">
        {{ Form::open(array('route' => 'service-add-or-update', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            @if (isset($service))
            {{ Form::hidden('id', $service->id) }}
            @endif
          <div class="form-group">
              <label for="access_type" class="col-sm-2 control-label">Access Type *</label>
              <div class="col-sm-9">
                  {{ Form::select('access_type', 
                                array('wimax' => 'WiMAX', 'lte' => 'LTE', 'wifi' => 'Wi-FI', 'radwin1000' => 'Radwin 1000', 'radwin2000' => 'Radwin 2000', 'ceragon' => 'Ceragon', 'fttb' => 'FTTB' ,'ftth' => 'FTTH' ,'ipran' => 'IPRAN'),
                                (empty($service->access_type)) ? Input::old('access_type') : $service->access_type,
                                array('id'=>'access_type', 'class'=>'form-control')) }}
              {{ $errors->first('access_type') }}
              </div>
          </div>
          <div class="form-group">
              <label for="name" class="col-sm-2 control-label">Name *</label>
              <div class="col-sm-9">
                  {{ Form::text('name',
                                (empty($service->name)) ? Input::old('name') : $service->name,
                                array('id'=>'name', 'placeholder'=>'e.g UC (Unified Communications)', 'class'=>'form-control')) }}
              {{ $errors->first('name') }}
              </div>
          </div>
          <div class="form-group">
            <label for="service-type" class="col-sm-2 control-label">Service Type *</label>
            <div class="col-sm-9">
                {{ Form::select('service-type', 
                                $service_type,
                                (empty($service->service_type)) ? Input::old('service-type') : $service->service_type,
                                array('id'=>'service-type', 'class'=>'form-control')) }}
            {{ $errors->first('service-type') }}
            </div>
          </div>
          <div class="form-group">
              <label for="is_active" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-9">
              {{ Form::checkbox("is-active",
                                $value = 1,
                                $checked = (empty($service->is_active)) ? Input::old('is-active') : true,
                                array('id'=>'is-active')) }}
              </div>
          </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-flat">{{ (empty($service)) ? "Save" : "Update" }}</button>
                </div>
            </div>
        {{ Form::close() }}
    </section>

@stop

@section('custom-script')
    {{ HTML::script('js/icheck.min.js') }}
    <script type="text/javascript">
        $(function(){
            var checkbox_toggler = function() {
                    var is_active = "{{ (empty($service->is_active)) ? 0 : 1 }}";
                    if (is_active == 1) {
                        $('#is_active').iCheck('check');
                    } else {
                        $('#is_active').iCheck('uncheck');
                    }
                },
                init = function() {
                    checkbox_toggler();
                }();
        })
    </script>
@stop