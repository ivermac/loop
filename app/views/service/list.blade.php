@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Service
        <small>Listing</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <table class="table">
        <thead>
            <tr>
                <th>Access Type</th>
                <th>Name</th>
                <th>Service Type</th>
                <th>Active</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($service as $row)
                <tr>
                    <td>{{ $row->access_type }}</td>
                    <td>{{ $row->name }}</td>
                    <td>{{ empty($row->service_type) ? "None" : ServiceType::find($row->service_type)->name }}</td>
                    <td>{{ (empty($row->is_active)) ? "No" : "Yes" }}</td>
                    <td>
                        @include('layouts.links', array('routes'=>['delete'=> 'service-delete',
                                                                   'edit' => 'service-edit',
                                                                   'view' => 'service-list'], 'id' => $row->id))
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ empty($service) ? "" : $service->links()}}
</section><!-- /.content -->
@stop
