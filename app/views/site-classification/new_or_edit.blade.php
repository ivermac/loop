@extends('layouts.main')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Site Classification
            <small>{{ (empty($site)) ? "New" : "Edit" }}</small>
        </h1>
    </section>

    <section class="content">
        {{ Form::open(array('route' => 'site-add-or-update', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')) }}
            @if (isset($site))
            {{ Form::hidden('id', $site->id) }}
            @endif
          <div class="form-group">
              <label for="region" class="col-sm-2 control-label">Region</label>
              <div class="col-sm-9">
                  {{ Form::select('region', 
                                array('Nairobi' =>'Nairobi', 'Thika' => 'Thika','Eldoret'=> 'Eldoret', 'Garissa'=> 'Garissa', 'Kakuma' => 'Kakuma', 'Kakamega' => 'Kakamega', 'Kericho' => 'Kericho', 'Kiambu' => 'Kiambu', 'Kilifi' => 'Kilifi', 'Kisumu' => 'Kisumu', 'Machakos' => 'Machakos', 'Malindi' => 'Malindi', 'Mandera' => 'Mandera', 'Mombasa' => 'Mombasa'),
                                (empty($site->region)) ? '' : $site->region,
                                array('id'=>'region', 'class'=>'form-control')) }}
              </div>
          </div>
          <div class="form-group">
            <label for="price" class="col-sm-2 control-label">Price</label>
            <div class="col-sm-9">
                {{ Form::selectRange('price', 
                                1000,100000, 
                                (empty($site->price)) ? '' : $site->price,
                                array('id'=>'price', 'class'=>'form-control')) }}
            </div>
          </div>
          <div class="form-group">
              <label for="is_active" class="col-sm-2 control-label">Active</label>
              <div class="col-sm-9">
              {{ Form::checkbox("is-active",
                                $value = 1,
                                $checked = (empty($site->is_active)) ? null : true,
                                array('id'=>'is-active')) }}
              </div>
          </div>
          <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-flat">{{ (empty($site)) ? "Save" : "Update" }}</button>
                </div>
            </div>
        {{ Form::close() }}
    </section>

@stop

@section('custom-script')
    {{ HTML::script('js/icheck.min.js') }}
    <script type="text/javascript">
        $(function(){
            var checkbox_toggler = function() {
                    var is_active = "{{ (empty($site->is_active)) ? 0 : 1 }}";
                    if (is_active == 1) {
                        $('#is_active').iCheck('check');
                    } else {
                        $('#is_active').iCheck('uncheck');
                    }
                },
                init = function() {
                    checkbox_toggler();
                }();
        })
    </script>
@stop