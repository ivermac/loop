@extends('layouts.main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        site
        <small>Listing</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <table class="table">
        <thead>
            <tr>
                <th>Region</th>
                <th>Price</th>
                <th>Active</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($site as $row)
                <tr>
                    <td>{{ $row->region }}</td>
                    <td>{{ $row->price }}</td>
                    <td>{{ (empty($row->is_active)) ? "No" : "Yes" }}</td>
                    <td>
                        @include('layouts.links', array('routes'=>['delete'=> 'site-delete',
                                                                   'edit' => 'site-edit',
                                                                   'view' => 'site-list'], 'id' => $row->id))
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ empty($site) ? "" : $site->links()}}
</section><!-- /.content -->
@stop
