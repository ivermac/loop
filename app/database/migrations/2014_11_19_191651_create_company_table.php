<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration {

	public function up()
	{
		Schema::create('company', function($table){
			$table->increments('id');
			$table->string('name', 100)->unique();
			$table->string('email', 60)->unique();
			$table->string('image', 100)->nullable();
			$table->integer('no_of_users')->default(5);
			$table->text('contact_details')->nullable();
			$table->text('description')->nullable();
			$table->boolean('is_active');
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('company');
	}

}
