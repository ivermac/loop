<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTypeTable extends Migration {

	public function up()
	{
		Schema::create('service_types', function($table) {
			$table->increments('id');
			$table->string('name', 100);
			$table->text('service_speed', 40);
			$table->boolean('is_active');
			$table->softDeletes();
		});
	}
	
	public function down()
	{
		Schema::dropIfExists('service_types');
	}

}
