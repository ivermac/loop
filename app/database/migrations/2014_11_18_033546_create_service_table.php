<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTable extends Migration {

	public function up()
	{
		Schema::create('services', function($table) {
			$table->increments('id');
			$table->string('access_type', 40);
			$table->string('name', 100);
			$table->integer('service_type');
			$table->boolean('is_active');
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('services');
	}

}
