<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteClassificationTable extends Migration {

	public function up()
	{
		Schema::create('site_classifications', function($table) {
			$table->increments('id');
			$table->string('region', 20);
			$table->integer('price');
			$table->boolean('is_active');
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('site_classifications');
	}

}
