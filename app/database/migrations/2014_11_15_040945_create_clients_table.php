<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('clients', function($table) {
			$table->increments('id');
			$table->string('circuit', 100);
			$table->string('account', 60);
			$table->string('name', 40);
			$table->string('email', 30);
			$table->string('phone', 20);
			$table->string('address', 30);
			$table->string('zip', 20);
			$table->integer('company');
			$table->integer('service');
			$table->integer('site');
			$table->string("gps", 100);
			$table->text("business_contacts");
			$table->text("other_info");
			$table->string("link", 100);
			$table->string("backup_link", 100);
			$table->string("router_model", 100);
			$table->string("switch_model", 100);
			$table->text("ip_address", 100);
			$table->text("subnet_mask", 100);
			$table->boolean("wifi");
			$table->string("wifi_device", 100)->nullable();
			$table->string("building", 100)->nullable();
			$table->string("floor", 20)->nullable();
			$table->string("wing", 20)->nullable();
			$table->string("room", 20)->nullable();
			$table->integer("added_by");
			$table->boolean('is_active');
			$table->softDeletes();
		});

	}

	public function down()
	{
		Schema::dropIfExists('clients');
	}

}
