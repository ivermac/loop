<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportTable extends Migration {

	public function up()
	{
		Schema::create('support', function($table) {
			$table->increments('id');
			$table->string('ticket', 100);
			$table->integer('customer');
			$table->integer('user');
			$table->integer('service');
			$table->text('incident');
			$table->text('incident_tag');
			$table->text('resolution')->nullable();
			$table->integer('status')->default(1);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('support');
	}

}
