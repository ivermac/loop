<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('support', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('ticket', 100);
            $table->integer('customer');
            $table->integer('user');
            $table->integer('service');
            $table->text('incident');
            $table->text('incident_tag')->nullable();
            $table->text('resolution')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('support');
	}

}
