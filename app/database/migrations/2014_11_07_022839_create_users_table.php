<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function($table) {
			$table->increments('id');
			$table->string('email', 100)->unique();
			$table->string('password', 100);
			$table->string('emp_no', 100)->unique();
			$table->integer('company');
			$table->string('remember_token', 100);
			$table->text('description');
			$table->integer('role');
			$table->boolean('is_active');
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('users');
	}

}
