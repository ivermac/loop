<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
        $this->call('SupportTableSeeder');
        $this->command->info('Support table seeded!');
        
        /*$this->call('UserTableSeeder');
        $this->command->info('User table seeded!');

        $this->call('ClientsTableSeeder');
        $this->command->info('Clients table seeded!');

        $this->call('CompanyTableSeeder');
        $this->command->info('Company table seeded!');

        $this->call('ServiceTypeTableSeeder');
        $this->command->info('Service Type table seeded!');

        $this->call('ServiceTableSeeder');
        $this->command->info('Service table seeded!');

        $this->call('SiteClassificationTableSeeder');
        $this->command->info('SiteClassification table seeded!');*/
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::create(
        	array(
                'id' => 1,
        		'email' => 'test@admin.com',
        		'password' => Hash::make('test'),
        		'emp_no' => 'INT540',
        		'company' => 1,
        		'remember_token' => 'remember-token',
        		'description' => 'this is a test account',
        		'role' => 1,
        		'is_active' => true,
        	)
        );
        User::create(
            array(
                'id' => 2,
                'email' => 'info@empire.co.ke',
                'password' => Hash::make('#MP!RE'),
                'emp_no' => 'Test2',
                'company' => 2,
                'remember_token' => 'remember-token',
                'description' => 'this is a test account',
                'role' => 2,
                'is_active' => true,
            )
        );
        User::create(
            array(
                'id' => 3,
                'email' => 'maina.wanjau@gmail.com',
                'password' => Hash::make('M@IN@'),
                'emp_no' => 'Test3',
                'company' => 3,
                'remember_token' => 'remember-token',
                'description' => 'this is a test account',
                'role' => 3,
                'is_active' => true,
            )
        );
        User::create(
            array(
                'id' => 4,
                'email' => 'embuvi@safaricom.co.ke',
                'password' => Hash::make('3mbuv!'),
                'emp_no' => 'Test4',
                'company' => 4,
                'remember_token' => 'remember-token',
                'description' => 'this is a test account',
                'role' => 3,
                'is_active' => true,
            )
        );
    }

}

class SupportTableSeeder extends Seeder {

    public function run()
    {
        
        DB::table('support')->delete();

        $faker = Faker\Factory::create();
         
        for ($i = 0; $i < 8; $i++) {
            Support::create(
                array(
                    'id' => $i + 1,
                    'ticket' => 'INC'.mt_rand(1000000, 9999999),
                    'customer' => $faker->numberBetween($min = 5, $max = 6),
                    'user' => $faker->numberBetween($min = 1, $max = 4),
                    // date($format = 'Y-m-d', $max = 'now') 
                    'service' => $faker->numberBetween($min = 1, $max = 4),
                    'status'  => $faker->randomElement($array = array (1, 2)),
                    'incident' => $faker->text,
                    'incident_tag' => $faker->randomElement($array = array('Power', 'MisConfiguration','Equipment', 'Transmission','Cable' ,'Problem X')),
                    'resolution' => $faker->text,
                )
            );
        }
        
    }

}

class ClientsTableSeeder extends Seeder {
    
    public function run()
    {
        DB::table('clients')->delete();                                                                                                                                     

        $faker = Faker\Factory::create();
                 
        for ($i = 1; $i < 5; $i++) {
            Client::create(
                array(
                    'id' => $i,
                    'circuit' => mt_rand(),
                    'account' => $faker->company,
                    'name' => $faker->name,
                    'email' => $faker->email,
                    'phone' => $faker->phoneNumber,
                    'address' => $faker->address,
                    'zip' => $faker->randomElement($array = array ('00200',
                                                                   '00300',
                                                                   '00400')),
                    'company' => $faker->randomElement($array = array (1, 2, 3, 4)),
                    'service' => $faker->randomElement($array = array (1, 2, 3, 4)),
                    'site' => $faker->randomElement($array = array (1, 2, 3, 4)),
                    'gps' => $faker->randomElement($array = array ('S10 25 36.5 E036 52 36.4',
                                                                   'S22 15 32.6 E072 32 11.4',
                                                                   'S01 42 76.5 E090 21 77.4')),
                    'business_contacts' => $faker->text,
                    'other_info' => $faker->text,
                    'link' => 'None',
                    'backup_link' => 'None',
                    'router_model' => $faker->randomElement($array = array ('2600',
                                                                            '4500',
                                                                            '5000')),
                    'switch_model' => 'None',
                    'ip_address' => $faker->ipv4,
                    'subnet_mask' => $faker->randomElement($array = array ('255.255.255.252',
                                                                           '255.255.255.231',
                                                                           '255.255.255.289')),
                    'wifi' => true,
                    'wifi_device' => $faker->randomElement($array = array ('2600',
                                                                           '4500',
                                                                           '5000')),
                    'building' => $faker->randomElement($array = array ('Blink Towers',
                                                                        'Msumbiji Towers',
                                                                        'Ekero Towers')),
                    'floor' => $faker->randomElement($array = array (1, 2, 3, 4)),
                    'wing' => $faker->randomElement($array = array ('A', 'B', 'None')),
                    'room' => $faker->randomElement($array = array (1, 2, 3, 4)),
                    'added_by' => $faker->randomElement($array = array (1, 2, 3, 4)),
                    'is_active' => true,
                )
            );
        }
    }
}

class CompanyTableSeeder extends Seeder {

    public function run()
    {
        DB::table('company')->delete();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 4; $i++) {
            Company::create([
                    'id' => $i + 1,
                    'name' => $faker->company,
                    'email' => $faker->companyEmail,
                    'image' => $faker->word,
                    'contact_details' => $faker->url,
                    'description' => $faker->text,
                    'is_active' => true,
            ]);
        }

    }
}

class ServiceTypeTableSeeder extends Seeder {

    public function run()
    {
        DB::table('service_types')->delete();

        $faker = Faker\Factory::create();

        $access_types = array('LTE', '3G', '2G', 'GPRS');
        for ($i = 0; $i < 4; $i++) {
            ServiceType::create([
                'id' => $i + 1,
                'name' => $access_types[$i],
                'service_speed' => '2048',
                'is_active' => true,
            ]);
        }
    }
}

class ServiceTableSeeder extends Seeder {
    public function run()
    {
        DB::table('services')->delete();

        $faker = Faker\Factory::create();
                 
        $access_types = array('LTE', '3G', '2G', 'GPRS');
        $names = array('LTE 2Mbps', '3G 2Mbps', '2G 2Mbps', 'GPRS 2Mbps');
        for ($i = 0; $i < 4; $i++) {
            Service::create([
                'id' => $i + 1,
                'access_type' => $access_types[$i],
                'name' => $names[$i],
                'service_type' => $faker->randomElement($array = array (1, 2, 3, 4)),
                'is_active' => true,
            ]);
        }
    }   
}

class SiteClassificationTableSeeder extends Seeder {
    public function run()
    {
        DB::table('site_classifications')->delete();

        $faker = Faker\Factory::create();
        
        for ($i = 0; $i < 4; $i++) {
            SiteClassification::create([
                'id' => $i + 1,
                'region' => $faker->randomElement($array = array ('Nairobi', 'Kisumu', 'Mombasa', 'Eldoret')),
                'price' => $faker->randomElement($array = array (1200, 1300, 1400, 1500)),
                'is_active' => true,
            ]);
        }
    }   
}
