<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Client extends Eloquent {
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    public function scopeSummaryList($query) {
        return $query->select('id',
                              'circuit',
                              'account',
                              'name',
                              'email',
                              'phone',
                              'address',
                              'company',
                              'gps',
                              'ip_address',
                              'subnet_mask',
                              'other_info',
                              'wifi',
                              'added_by',
                              'is_active')->get();
    }

}