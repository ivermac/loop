<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class SiteClassification extends Eloquent {
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'site_classifications';
    public $timestamps = false;

}