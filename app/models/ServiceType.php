<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ServiceType extends Eloquent {
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'service_types';
    public $timestamps = false;

}