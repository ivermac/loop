<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Company extends Eloquent {
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'company';
    public $timestamps = false;

}