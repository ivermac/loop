<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Service extends Eloquent {
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'services';
    public $timestamps = false;

}