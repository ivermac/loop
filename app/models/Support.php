<?php
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Support extends Eloquent {
    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'support';
    protected $hidden = array('deleted_at');
    //public $timestamps = false;

}