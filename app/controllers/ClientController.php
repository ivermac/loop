<?php

class ClientController extends BaseController {

    public function __construct() {
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('only'=>array('delete', 
                                                        'edit',
                                                        '_list',
                                                        '_new',
                                                        'add_or_update',
                                                        '_dashboard')));
        
        $this->companies = Company::lists('name' , 'id');
        $this->service = Service::lists('name' , 'id');
        $this->site = SiteClassification::lists('region' , 'id');
    }

    public function delete($id) {
        $client = Client::find($id);
        if (empty($client)) {
            return Redirect::route('client-list')->with('splash-message', 'Record not available');
        }
        $client->delete();
        return Redirect::route('client-list')->with('splash-message', 'Record deleted');
    }

    public function edit($id) {
        $client = Client::find($id);
        if (empty($client)) {
            return Redirect::route('client-list')->with('splash-message', 'Record not available');
        }
        return View::make('client/new_or_edit')
                    ->with('company_select', $this->companies)
                    ->with('service', $this->service)
                    ->with('site', $this->site)
                    ->with('client', $client);
    }

    public function view($id) {
        $client = Client::find($id);
        if (empty($client)) {
            return Redirect::route('client-list')->with('splash-message', 'Record not available');
        }
        return View::make('client/detail_view')
                    ->with('company_select', $this->companies)
                    ->with('service', $this->service)
                    ->with('site', $this->site)
                    ->with('client', $client);
    }

    public function add_or_update(){
        $id = Input::get('id');
        $wifi = Input::get('wifi');
        $is_active = Input::get('is-active');
        
        $client = (empty($id)) ? new Client : Client::find($id) ;
        $wifi = (empty($wifi)) ? false : true ;
        $is_active = (empty($is_active)) ? false : true ;

        $client->circuit = Input::get('circuit');
        $client->account = Input::get('account');
        $client->name = Input::get('client-name');
        $client->email = Input::get('email');
        $client->phone = Input::get('phone');
        $client->address = Input::get('address');
        $client->zip = Input::get('zip');
        $client->company = Input::get('company');
        $client->service = Input::get('service');
        $client->site = Input::get('site');
        $client->gps = Input::get('gps');
        $client->business_contacts = Input::get('business-contacts');
        $client->other_info = Input::get('other-info');
        $client->link = Input::get('link');
        $client->backup_link = Input::get('backup-link');
        $client->router_model = Input::get('router-model');
        $client->switch_model = Input::get('switch-model');
        $client->ip_address = Input::get('ip-address');
        $client->subnet_mask = Input::get('subnet-mask');
        $client->wifi = $wifi;
        $client->wifi_device = Input::get('wifi-device');
        $client->building = Input::get('building');
        $client->floor = Input::get('floor');
        $client->wing = Input::get('wing');
        $client->room = Input::get('room');
        $client->added_by = Auth::user()->id;
        $client->is_active = $is_active;
        //save records
        $client->save();
        return Redirect::route('client-list')->with('splash-message', 'Record successfully added');
    }

    public function _list(){
        $client = Client::paginate(10);
        return View::make('client/list')
                ->withCompanies($this->companies)
                ->with('client', $client)
                ->withSearch('support-search');
    }

    public function search() {
        $query = Request::get('q');
        //var_dump($query);
      
        $client = $query
            ? Client::where('circuit', 'LIKE', "%$query%")->paginate(10)
            : $client = Client::paginate(10);   
        
        return View::make('client.list')
                    ->withCompanies($this->companies)
                    ->withClient($client);
    }

    public function _new(){
        return View::make('client/new_or_edit')
                    ->with('service', $this->service)
                    ->with('site', $this->site)
                    ->with('company_select', $this->companies);
    }
}
