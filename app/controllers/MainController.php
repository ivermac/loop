<?php

class MainController extends BaseController {

    public function home() {
        /*$rs = DB::select( DB::raw("PRAGMA table_info(clients)") );
        var_dump("hey, t'sup!!!!");*/
        if (Auth::check()) {
            # code...
            return Redirect::to('support/list');
        } else {
            return View::make('login');
        }
    }

    public function authenticate() {
        $email = Input::get('email');
        $password = Input::get('password');
        if (Auth::attempt(array('email' => $email, 'password' => $password))) {
            return Redirect::intended('support/list');
        }
        return Redirect::to('/')
              ->with('message', 'Your email/password combination was incorrect')
              ->withInput();
    }

    public function logout() {
        Auth::logout();
        return Redirect::to('/')
            ->with('message', 'Your are now logged out!')
            ->with('custom-alert', 'alert-message-warning');
    }
    public function _export() {
        $data = Support::all();
        Excel::create('testexport', function($excel) use ($data){
           $excel->sheet('Support_tb', function($sheet) use ($data){
                $support_cols=Schema::getColumnListing('support');
                $sheet->appendRow($support_cols, null, 'A1', true, true);
                foreach ($data as $row) {
                    $sheet->appendRow($row->toArray(), null, 'A2', true, true);
            } 
           });
        })->store('csv', public_path().DIRECTORY_SEPARATOR.'csv');
        Log::info('<<<<<<<<<< Export Successful >>>>>>>>>>>>>>>>>>>');
        return Redirect::back('/');
    }

    public function _importClients() {
        $filename = 'testexport.csv';
        $table_col = implode(',', Schema::getColumnListing('support'));
        //var_dump($table_col);
        $file_path = addslashes(
            public_path()
            .DIRECTORY_SEPARATOR."csv"
            .DIRECTORY_SEPARATOR.$filename
        );

        $query = "LOAD DATA INFILE '$file_path' 
                      INTO TABLE support 
                      FIELDS TERMINATED BY ',' 
                      LINES TERMINATED BY '\\r\\n' 
                      IGNORE 1 LINES 
                      ('ticket,customer,user,service,incident,incident_tag,resolution,status,created_at,updated_at,deleted_at');";
        var_dump($query);
         return DB::connection()->getpdo()->exec($query);
    }

    public function _importClientsMat() {
        
    }

}
