<?php

class UserController extends BaseController {
    public function __construct(){
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('only'=>array('delete', 
                                                        'edit',
                                                        '_list',
                                                        '_getnewpassword',
                                                        '_postnewpassword',
                                                        '_new',
                                                        'add_or_update',
                                                        '_dashboard')));
        
        $this->company  = Company::lists('name', 'id');
        $this->rules = array(
            'email' => 'required|email|unique:users',
            'company' => 'required',
            'emp_no' => 'required|unique:users',
            'role' => 'required',
        );
        $this->rules_update = array(
            'email' => 'required|email',
            'company' => 'required',
            'emp_no' => 'required',
            'role' => 'required',
        );
        $this->password_change_rules = array(
            'password'      => 'required|min:3',
            'password_new'  => 'required|min:3',
            'password_new_confirm' => 'required|min:3|same:password_new'
        );
        $this->roles = array('1' => 'Super Admin' ,'2' => 'Company Admin', '3' => 'Normal User');
    }

    public function delete($id) {
        $user= User::find($id);
        $user->delete();
        return Redirect::route('user-list')->with('splash-message', 'Record deleted');
    }

    public function edit($id) {
        $user = User::find($id);
        if (empty($user)) {
            return Redirect::route('user-list')
                    ->with('splash-message', 'Record not available');
        }
        return View::make('user/new_or_edit')
                    ->with('user', $user)
                    ->with('company', $this->company);
    }

    public function add_or_update(){
        $id      = Input::get('id');
        $password = Input::get('password');
        $is_active  = (empty($is_active)) ? false : true ;
        $email    = Input::get('email');
        if(empty($id)){
            $User = new User;
            //check if user is new and validate separately
            $validator = Validator::make(Input::all() , $this->rules);
            if($validator->fails()) {
                return Redirect::route('user-new')
                    ->with('splash-message', 'Some Fields Missing')
                    ->withInput()
                    ->withErrors($validator);
            }
            $code = str_random(60);
            $User->code = $code; //code for mail activation
            $User->is_active =  0; //deactivate by default
            $User->email    = $email;
            //send mail
            $this->_sendmailactivation($code, $email);

        }else {
            $User = User::find($id);
            //check if user is new and validate separately
            $validator = Validator::make(Input::all() , $this->rules_update);
            if ($validator->fails()) {
                return Redirect::route('user-new')
                    ->with('splash-message', 'Some Fields Missing')
                    ->withInput()
                    ->withErrors($validator);
            }
            $User->email    = User::find($id)->email;
            $User->is_active= $is_active;
        }
        $User->password = Hash::make($password);
        $User->emp_no   = Input::get('emp_no');
        $User->company  = Input::get('company');
        $User->remember_token = 'remember_token';
        $User->description    = Input::get('description');
        $User->role     = Input::get('role');
        $User->save();
        
        return Redirect::route('user-list')->with('splash-message', 'Training record successfully added');
    }

    public function _sendmailactivation($code, $email){
        
        $code = $code;
        $email = $email;
        $messageData = array(
            'link' => URL::route('user-activate', $code),
            'code' => $code, 
            'email' => $email 
        );
        if ($email) {
            Mail::send('emails.auth.email_activation', $messageData, function($message) use ($messageData) {   
                $message->to($messageData['email'])->subject('Your account has been created');
            });
            Log::info('User Account Activation-Mail Sent>>>>>>>>');
        } else {
            return Redirect::route('user-list');
        }
    }

    public function _activateaccount($code) {
        $user = User::where('code', '=', $code)->where('is_active', '=', 0);
        if ($user->count()) {
            $user = $user->first();
            $user->is_active = true;
            $user->code = ''; //reset the user activation code

            if($user->save()){
                Log::info('<<<<<<<<<<<<<<<<<<< Account successfully activated >>>>>>>>>>>>>>>>>>');
                Auth::logout();
                return Redirect::route('home')
                        ->with('global', 'Activated ! You can Now sign in!');
            }
            Log::info('<<<<<<<<<<<<<<<<<<< Account activation Unsuccessful >>>>>>>>>>>>>>>>>>');
        }
    }

    public function _list(){
        $user = User::all();
        return View::make('user/list')
                ->withCompany($this->company)
                ->withRole($this->roles)
                ->with('user', $user);
    }

    public function _new(){
        return View::make('user/new_or_edit')->with('company', $this->company);
    }

    public function _getnewpassword() {
        return View::make('user.change_password');
    }

    public function _postnewpassword() {
        $validator = Validator::make(Input::all(), $this->password_change_rules);
        if($validator->fails()) {
            return Redirect::route('user-change-password')
                            ->withErrors($validator)
                            ->withFail('Your New Password Doesnt match with the confirm Password ');   
        } else {
            $user                   = User::find(Auth::user()->id);
            $old_password           = Input::get('password'); 
            $new_password           = Input::get('password_new');
            if(Hash::check($old_password, $user->password)) {
                $user->password = Hash::make($new_password);
                if ($user->save()) {
                    return  Redirect::route('user-change-password')
                        ->with('success' ,'Your password was successfully changed');
                } else {
                    return  Redirect::route('user-change-password')
                        ->with('fail', 'Failed to save password')
                        ->withInput();
                }
            } else {
                return Redirect::route('user-change-password')
                  ->with('fail', 'Your password is incorrect')
                  ->withInput();
            }
        }   
   }
}