<?php

class ServiceController extends BaseController {

    public function __construct(){
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('only'=>array('delete', 
                                                        'edit',
                                                        '_list',
                                                        '_new',
                                                        'add_or_update',
                                                        '_dashboard')));
        
        $this->service_type = ServiceType::lists('name' , 'id');
        $this->rules = array(
            'access_type'   => 'required',
            'service-type'  => 'required',
            'name'          => 'required'
        );
    }

    public function delete($id) {
        $service = Service::find($id);
        if (empty($service)) {
            return Redirect::route('service-list')->with('splash-message', 'Record not available');
        }
        $service->delete();
        return Redirect::route('service-list')->with('splash-message', 'Record deleted');
    }

    public function edit($id) {
        $service = Service::find($id);
        if (empty($service)) {
            return Redirect::route('service-list')->with('splash-message', 'Record not available');
        }
        return View::make('service/new_or_edit')
                    ->with('service', $service)
                    ->with('service_type', $this->service_type);
    }

    public function add_or_update(){
    	$id = Input::get('id');
        $is_active = Input::get('is-active');
		$is_active = (empty($is_active)) ? false : true ;
       
        if (empty($id)) {
            $service = new Service;
        } else {
            $service = Service::find($id);
        }
        $validator = Validator::make(Input::all() , $this->rules);
        if($validator->fails())
        {
            return Redirect::route('service-new')
                    ->with('splash-message', 'Some Fields Missing')
                    ->withInput()
                    ->withErrors($validator);
        }else {
            $service->access_type = Input::get('access_type');
            $service->name = Input::get('name');
            $service->service_type = Input::get('service-type');
            $service->is_active = $is_active;
            $service->save();
        }

        return Redirect::route('service-list')->with('splash-message', 'Training record successfully added');
    }

    public function _list(){
        $service = Service::paginate(10);
        return View::make('service/list')
                ->with('service', $service);
    }

    public function _new(){
        return View::make('service/new_or_edit')->with('service_type', $this->service_type);
    }
}
