<?php

class ServiceTypeController extends BaseController {

    public function __construct(){
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('only'=>array('delete', 
                                                        'edit',
                                                        '_list',
                                                        '_new',
                                                        'add_or_update',
                                                        '_dashboard')));
        
        $this->rules = array(
            'name'          => 'required',
            'service-speed' => 'required'
        );
    }
    public function delete($id) {
        $service_type = ServiceType::find($id);
        $service_type->delete();
        return Redirect::route('service-type-list')->with('splash-message', 'Record deleted');
    }

    public function edit($id) {
        $service_type = ServiceType::find($id);
        if (empty($service_type)) {
            return Redirect::route('service-type-list')->with('splash-message', 'Record not available');
        }
        return View::make('service-type/new_or_edit')
                    ->with('service_type', $service_type);
    }

    public function add_or_update(){
        $id = Input::get('id');
        $is_active = Input::get('is-active');
        $is_active = (empty($is_active)) ? false : true ;
        if (empty($id)) {
            $service_type = new ServiceType;
        } else {
            $service_type = ServiceType::find($id);
        }
        $validator = Validator::make(Input::all(), $this->rules);
        if($validator->fails())
        {
            return Redirect::route('service-type-new')->withErrors($validator)->withInput();
        }else {
            $service_type->name = Input::get('name');
            $service_type->service_speed = Input::get('service-speed');
            $service_type->is_active = $is_active;
            $service_type->save();
        }

        return Redirect::route('service-type-list')->with('splash-message', 'Record successfully added');
    }

    public function _list(){
        $service_type = ServiceType::paginate(10);
        return View::make('service-type/list')
                ->with('service_type', $service_type);
    }

    public function _new(){
        return View::make('service-type/new_or_edit');
    }
}
