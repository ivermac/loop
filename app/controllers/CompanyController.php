<?php

class CompanyController extends \BaseController {
    public function __construct(){
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('only'=>array('delete', 
                                                        'edit',
                                                        '_list',
                                                        '_new',
                                                        'add_or_update',
                                                        '_dashboard')));
        
        $this->rules = array(
            'name'  => 'required',
            'email' => 'required|email|unique:company'
        );
    }

	public function delete($id) {
        $company = Company::find($id);
        if (empty($company)) {
            return Redirect::route('company-list')->with('splash-message', 'Record not available');
        }
        $company->delete();
        return Redirect::route('company-list', $data)->with('splash-message', 'Record deleted');
    }

    public function edit($id) {
        $company = Company::find($id);
        if (empty($company)) {
            return Redirect::route('company-list')->with('splash-message', 'Record not available');
        }
        return View::make('company/new_or_edit')
                    ->with('company', $company);
    }

    public function add_or_update(){
        var_dump("i was here");
        $id = Input::get('id');
        $is_active = Input::get('is-active');
        $is_active = (empty($is_active)) ? false : true ;
        if (empty($id)) {
            $company = new Company;
        } else {
            $company = Company::find($id);
        }
        $validator = Validator::make(Input::all(), $this->rules);
        if($validator->fails())
        {
            return Redirect::route('company-new')
                    ->with('splash-message', 'Some Fields Missing')
                    ->withInput()
                    ->withErrors($validator);
        } else {
            $company->name = Input::get('name');
            $company->email = Input::get('email');
            $company->image = Input::get('image');
            $company->no_of_users  = Input::get('users_no');
            $company->contact_details = Input::get('contact_details');
            $company->description = Input::get('description');
            $company->is_active = $is_active;
            $company->save();
        }
        return Redirect::route('company-list')->with('splash-message', 'Record successfully added');
    }

    public function _list(){
        $company = Company::paginate(10);
        return View::make('company/list')
                ->with('company', $company);
    }

    public function _new(){
        return View::make('company/new_or_edit');
    }
}
