<?php
use Carbon\Carbon;

class SupportController extends BaseController {

    public function __construct(){
        $this->beforeFilter('csrf', array('on'=>'post'));
        $this->beforeFilter('auth', array('only'=>array('delete', 
                                                        'edit',
                                                        '_list',
                                                        '_new',
                                                        'export',
                                                        'report',
                                                        'add_or_update',
                                                        '_dashboard')));

        $this->customer =  Client::lists('name' , 'id');
        $this->service = Service::lists('name', 'id');
        $this->access_types = Service::lists('access_type', 'id');
        $this->users = User::lists('email', 'id');
        $this->company = Company::lists('name', 'id');
        $this->rules = array(
            'incident'      => 'required',
            'customer' => 'required',
            'user' => 'required'
        ); 
    }

    public function daily_report($begin_date=null){
        // date format should be yyyy-mm-dd
        if (empty($begin_date))
            $results = Support::all()->toArray();
        else
            $results = Support::where('updated_at', 'LIKE', '%$begin_date%')->get()->toArray();

        $records = [];
        $total_interval = 0;
        $row_count = 0;
        foreach ($results as $row) {
            $datetime2 = date_create($row['updated_at']);
            $datetime1 = date_create($row['created_at']);
            $interval = date_diff($datetime1, $datetime2);
            $duration = $interval->format('%h');
            $total_interval = $total_interval + intval($duration);
            $row_count++;
            array_push($records, ['ticket' => $row['ticket'], 'duration' => $duration]);
        }
        $average = ($total_interval == 0) ? 0 : $total_interval/$row_count;

        return View::make('support/report')->with("record", $records)
                                           ->with("average", $average);
    }

    public function weekly_report($begin_date=null,$end_date=null){
        // date format should be yyyy-mm-dd
        if (empty($begin_date) || empty($end_date))
            $results = Support::all()->toArray();
        else
            $results = Support::whereBetween('updated_at', array($begin_date, $end_date))->get()->toArray();
        
        $records = [];
        $total_interval = 0;
        $row_count = 0;
        foreach ($results as $row) {
            $datetime2 = date_create($row['updated_at']);
            $datetime1 = date_create($row['created_at']);
            $interval = date_diff($datetime1, $datetime2);
            $duration = $interval->format('%h');
            $total_interval = $total_interval + intval($duration);
            $row_count++;
            array_push($records, ['ticket' => $row['ticket'], 'duration' => $duration]);
        }
        $average = ($total_interval == 0) ? 0 : $total_interval/$row_count;

        return View::make('support/report')->with("record", $records)
                                           ->with("average", $average);
    }

    public function per_user_assigned(){
        $rs = DB::table('support as s')
                ->select(
                    DB::raw(
                       'u.email as email, 
                        count(s.id) as total_issues'
                    )
                )
                ->join('users as u', 'u.id', '=', 's.user')
                ->groupBy('u.email')
                ->get();

        return View::make('support/per-user-assigned-report')->with("results", $rs);
        
    }
    public function per_ticket_assigned(){
        $rs = DB::table('support as s')
                ->select(
                    DB::raw(
                       'u.email as email, 
                        s.ticket as ticket'
                    )
                )
                ->join('users as u', 'u.id', '=', 's.user')
                ->groupBy('s.ticket')
                ->get();
        return View::make('support/per-ticket-assigned-report')->with("results", $rs);
    }

    public function export($extension="csv", $model="support") {
        $data = Support::all()->toArray();
        Excel::create($model, function($excel) use ($data, $model){
            $excel->sheet($model."-sheet-1", function($sheet) use ($data, $model){
                $support_cols=Schema::getColumnListing($model);
                $sheet->appendRow($support_cols, null, 'A1', true, true);
                foreach ($data as $row) {
                    $sheet->appendRow($row, null, 'A2', true, true);
            } 
           });
        })->export($extension, public_path().DIRECTORY_SEPARATOR.'csv');
    }

    public function delete($id) {
        $support = Support::find($id);
        if (empty($support)) {
            return Redirect::route('support-list')->with('splash-message', 'Record not available');
        }
        $support->delete();
        return Redirect::route('support-list')->with('splash-message', 'Record deleted');
    }

    public function edit($id) {
        $support = Support::find($id);
        if (empty($support)) {
            return Redirect::route('support-list')->with('splash-message', 'Record not available');
        }
        return View::make('support/new_or_edit')
                    ->with('support', $support)
                    ->with('customer', $this->customer)
                    ->with('users', $this->users)
                    ->with('service', $this->service);
    }

    public function view($id){
        $support = Support::find($id);
        if (empty($support)) {
            return Redirect::route('support-list')->with('splash-message', 'Record not available');
        }
        $client_details = Client::find($support->customer);
        $client_service = ServiceType::find($client_details->service);
        $client_site_details = SiteClassification::find($client_details->site);
        $client_service_type = Service::find($client_details->service);
        return View::make('support/detail_view')
                    ->with('support', $support)
                    ->with('customer', $this->customer)
                    ->with('users', $this->users)
                    ->with('service', $this->service)
                    ->with('company', $this->company)
                    ->with('access_types', $this->access_types)
                    ->with('client_details', $client_details)
                    ->with('client_service', $client_service)
                    ->with('client_site_details', $client_site_details);
    }

    public function add_or_update(){
        $id = Input::get('id');
        if (empty($id)) {
            $support = new Support;
            $support->ticket = 'INC'.mt_rand(1000000, 9999999);
        } else {
            $support = Support::find($id);
            $support->status  = Input::get('status');
            $support->resolution = Input::get('resolution');
        }
        $validator = Validator::make(Input::all(), $this->rules);
        if($validator->fails())
        {
            return Redirect::route('support-new')->withErrors($validator)->withInput();
        } else {
            $support->customer = Input::get('customer');
            $support->user = Input::get('user');
            $support->service = Input::get('service');
            $support->incident = Input::get('incident');
            $support->incident_tag = Input::get('incident_tag');
            $support->save();
        }

        $email = User::find($support->user)->email;
        //only send mail if new ticket has been sent
        if (isset($id)) {
            //send mail
            $this->_sendmailnotification($support->ticket, $support->id,  $email);
        }
        return Redirect::route('support-list')->with('splash-message', 'Record successfully added');
    }

    public function _list(){
        if (Auth::user()->role <= 2) {
            # code...
            $support = Support::orderBy('created_at', 'DESC')->paginate(10);
        } else{
            $support = Support::orderBy('created_at', 'DESC')->where('user', '=', Auth::user()->id)->paginate(10);
        }
        return View::make('support/list')
                ->with('support', $support)
                ->with('users', $this->users)
                ->with('services', $this->service)
                ->with('customers', $this->customer);
    }

    public function _new(){
        $support = Support::all();
        return View::make('support/new_or_edit')
                ->with('customer', $this->customer)
                ->with('users', $this->users)
                ->with('service', $this->service);
    }

    public function _dashboard(){
        $supported_clients = Support::select(DB::raw('count(*) as client_count, _company.name'))
                            ->join('users as _users', '_users.id', '=', 'support.user')
                            ->join('company as _company', '_company.id', '=', '_users.company')
                            ->groupBy('support.user')
                            ->get()
                            ->toJson();
        $data = json_encode($supported_clients);
        return View::make('support/dashboard')
                        ->with('supported_clients', $supported_clients);
    }

    public function search() {
        $query = Request::get('q');
        //var_dump($query);
        $support = $query
            ? Support::where('ticket', 'LIKE', "%$query%")->paginate(10)
            : $support = Support::paginate(10);   
        
        return View::make('support.list')
                    ->withSupport($support)
                    ->with('search_route', 'support-search')
                    ->with('users', $this->users)
                    ->with('services', $this->service)
                    ->with('customers', $this->customer);
    }

    public function _sendmailnotification($tt, $id, $email){
        
        $tt = $tt;
        $id = $id;
        $email = $email;
        $messageData = array(
            'link' => URL::route('support-view', $id) ,
            'code' => str_random(60),
            'tt' => $tt, 
            'email' => $email 
        );
        if ($email) {
            Mail::send('emails.auth.notification', $messageData, function($message) use ($messageData) {   
                $message->to($messageData['email'])->subject('Ticket Submitted You');
            });
            Log::info('Mail Sent>>>>>>>>');
        } else {
            return Redirect::back();
        }
    }
}
