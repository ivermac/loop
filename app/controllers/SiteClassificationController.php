<?php

class SiteClassificationController extends \BaseController {

	public function delete($id) {
        $site = SiteClassification::find($id);
        $site->delete();
        return Redirect::route('site-list')->with('splash-message', 'Record deleted');
    }

    public function edit($id) {
        $site = SiteClassification::find($id);
        if (empty($site)) {
            return Redirect::route('site-list')->with('splash-message', 'Record not available');
        }
        return View::make('site-classification/new_or_edit')
                    ->with('site', $site);
    }

    public function add_or_update(){
        $id = Input::get('id');
        $is_active = Input::get('is-active');
        $is_active = (empty($is_active)) ? false : true ;
        if (empty($id)) {
            $site = new SiteClassification;
        } else {
            $site = SiteClassification::find($id);
        }
        $site->region = Input::get('region');
        $site->price = Input::get('price');
        $site->is_active = $is_active;
        $site->save();

        return Redirect::route('site-list')->with('splash-message', 'Record successfully added');
    }

    public function _list(){
        $site = SiteClassification::paginate(10);
        return View::make('site-classification/list')
                ->with('site', $site);
    }

    public function _new(){
        return View::make('site-classification/new_or_edit');
    }

}
