@servers(['web' => 'root@206.72.195.219'])

@task('deploy', ['on' => 'web'])
	cd /var/www/loop
    git pull origin master
    php artisan db:seed
@endtask
